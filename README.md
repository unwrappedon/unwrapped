Unwrapped sells environmentally, eco friendly products to promote zero waste living. we care about where the products we carry are made and what goes into them. We offer Canadian made products at competitive prices.

Address: 101 Kent St W, Lindsay, ON K9V 2Y5, Canada

Phone: 705-701-8703

Website: http://www.unwrappedkawartha.com
